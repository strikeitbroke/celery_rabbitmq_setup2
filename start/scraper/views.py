from time import sleep
from scraper.models import Post
from scraper.serializers import PostSerializer
from rest_framework import status
from rest_framework.views import APIView
from rest_framework.response import Response


class ScraperList(APIView):

    def get(self, request):
        posts = Post.objects.all()
        s = PostSerializer(
            posts,
            many=True,
            context={"request": request})
        return Response(s.data,
                        status=status.HTTP_200_OK)

    def post(self, request, format=None):
        print("scraping in process.")
        sleep(5)
        posts = [
            {
                "title": "Assistant Chef",
                "company": "XYZ",
                "location": "Seattle"
            },
            {
                "title": "Head Chef",
                "company": "ABC",
                "location": "Seattle"
            },
        ]
        print("scraping done.")
        for post in posts:
            s = PostSerializer(data=post)
            if s.is_valid():
                s.save()
                return Response({"result": "success"},
                                status=status.HTTP_201_CREATED)
            else:
                return Response({"result": "failed"},
                                status=status.HTTP_400_BAD_REQUEST)


# Create your views here.
