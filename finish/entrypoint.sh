#!/bin/sh
python manage.py makemigrations --no-input
python manage.py migrate --no-input
python manage.py collectstatic --no-input

celery -A core worker --loglevel=info > celery.log 2>&1 &
gunicorn core.wsgi:application --bind 0.0.0.0:8000 --timeout 6