from time import sleep
from celery import shared_task
from celery.signals import task_success
from scraper.serializers import PostSerializer


@shared_task
def scraping_task(title, location):
    print("scraping in process.")
    sleep(5)
    posts = [
        {
            "title": "Assistant Chef",
            "company": "XYZ",
            "location": "Seattle"
        },
        {
            "title": "Head Chef",
            "company": "ABC",
            "location": "Seattle"
        },
    ]
    print("scraping done.")
    return posts


@task_success.connect(sender=scraping_task)
def save_to_db_task(sender=None, **kwargs):
    print("reach store to db!!!!!!!!!")
    posts = kwargs.get("result")
    # requests.post("http://127.0.0.1:8000/api/scraper/save",
    #               json={"posts": data})
    for post in posts:
        s = PostSerializer(data=post)
        if s.is_valid():
            s.save()
    print("save data success!")
