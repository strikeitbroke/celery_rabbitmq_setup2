from time import sleep
from rest_framework import status
from rest_framework.views import APIView
from rest_framework.response import Response
from scraper.serializers import PostSerializer
from scraper.models import Post
from scraper.tasks import scraping_task


class ScraperList(APIView):

    def get(self, request):
        posts = Post.objects.all()
        s = PostSerializer(
            posts,
            many=True,
            context={"request": request})
        return Response(s.data,
                        status=status.HTTP_200_OK)

    def post(self, request, format=None):
        task_id = scraping_task.delay("Chef", "Seattle")
        return Response({"result": str(task_id)},
                        status=status.HTTP_201_CREATED)
