from django.urls import path
from scraper import views

urlpatterns = [
    path('scraper', views.ScraperList.as_view()),
]
